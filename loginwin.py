import tkinter as tk
from PIL import ImageTk, Image
import re
from datetime import datetime
from database_code import update_attendance,fetch_attendance,fetch_attendance_prc
from tkinter import messagebox
from tkinter import ttk
def run_once(f):
    def wrapper(*args, **kwargs):
        if not wrapper.has_run:
            wrapper.has_run = True
            return f(*args, **kwargs)
    wrapper.has_run = False
    return wrapper

class Login():
        def login_window(self,data):
                global dt
                self.dt = data
                self.root1 = tk.Tk()
                self.root1.title("Attendance System")
                name,r_no = data[1],data[2]
                pic= str(name)+"__"+str(r_no)+".jpg"

                lb = tk.Label(self.root1,text="Welcome {} !!!".format(name), font=("Verdena",20), fg="green",bg="white")
                lb.pack(ipadx=10,ipady=5,padx=10,pady=10,fill="x")
                
                myimg = "images/"+pic
                photo_location = Image.open(myimg)
                tkpi = ImageTk.PhotoImage(photo_location)
                label_image = tk.Label(self.root1, image=tkpi)
                label_image.image = tkpi
                label_image.pack()

                btn =tk.Button(self.root1,text="Click to Continue",bg="green",fg="white",bd=0,command=self.user_profile)
                btn.pack(ipadx=10,ipady=5,padx=10,pady=10,fill="x")
        
                btn1 =tk.Button(self.root1,text="Click to Cancel",bg="red",fg="white",bd=0,command=exit)
                btn1.pack(ipadx=10,ipady=5,padx=10,pady=10,fill="x")

                self.root1.geometry("500x500")
                self.root1.resizable(0,0)

        def user_profile(self):
                self.root1.destroy()
                self.profile = tk.Tk()
                self.profile.title("Attendance System: {}'s Profile".format(self.dt[1]))

                lf1 = tk.LabelFrame(self.profile,text="Profile",bg="pink",height=200)
                lf1.pack(side="left",fill="both",expand="yes")

                lf2 = tk.LabelFrame(self.profile)
                lf2.pack(side="right",fill="both",expand="yes")

                ##### Adding widgets
                name,r_no = self.dt[1],self.dt[2]
                pic= str(name)+"__"+str(r_no)+".jpg"
                myimg = "images/"+pic
                photo_location = Image.open(myimg)
                tkpi = ImageTk.PhotoImage(photo_location)
                label_image = tk.Label(lf1, image=tkpi)
                label_image.image = tkpi
                label_image.pack()

                #To separate registration date and time
                dt=list(map(lambda x:str(x).split(" ") if re.match('\d{4}-\d{2}-\d{2}', str(x)) else x,self.dt))
                ls=[]
                for i in dt:
                        if type(i)==type([]):
                                for j in i:
                                        ls.append(j.split(".")[0])
                        else:
                                ls.append(i)
                cv = tk.Canvas(lf1,bg="lightgreen")
                cv.pack(pady=5,ipady=5,ipadx=1)
            
                cols = ["User ID","Name","Roll Number","Course","Branch","Semester","Registration Date","Registreation Time"]
                c=0
                for k,v in zip(cols,ls):
                        lbl="lbl"+str(c)
                        lbl= tk.Label(cv,height=1,text=k.upper(),anchor="w",font=("Helvetica",14),relief="ridge", borderwidth=2,bg="lightgreen")
                        lbl.grid(row = c,column = 0, sticky="nesw")
                        lbl= tk.Label(cv,height=1,text=v,anchor="s",font=("ms serif",14),relief="ridge", borderwidth=2,bg="lightgreen")
                        lbl.grid(row = c,column = 1, sticky="nesw")
                        c+=1
                mylf = tk.LabelFrame(lf1,text="My Attendance")
                mylf.pack(fill="x",pady=5,ipady=5,ipadx=1,padx=2)
                my_attendance = fetch_attendance_prc(self.dt[0])
                total = my_attendance["total"]
                present = my_attendance["present"]
                percentage = round((present/total)*100,2)

                atlbl1 = tk.Label(mylf,text="Total Classes: ",height=1,anchor="w",font=("Helvetica",12),relief="ridge", borderwidth=2,bg="white")
                atlbl2 = tk.Label(mylf,text=total,height=1,anchor="s",font=("Helvetica",10),relief="ridge", borderwidth=2,bg="white")
                atlbl3 = tk.Label(mylf,text="Present Days: ",height=1,anchor="w",font=("Helvetica",12),relief="ridge", borderwidth=2,bg="white")
                atlbl4 = tk.Label(mylf,text=present,height=1,anchor="s",font=("Helvetica",10),relief="ridge", borderwidth=2,bg="white")
                atlbl5 = tk.Label(mylf,text="Attendance in (%): ",height=1,anchor="w",font=("Helvetica",12),relief="ridge", borderwidth=2,bg="white")
                atlbl6 = tk.Label(mylf,text=percentage,height=1,anchor="s",font=("Helvetica",10),relief="ridge", borderwidth=2,bg="white")

                atlbl1.grid(row=0,column=0, sticky="nesw")
                atlbl2.grid(row=0,column=1, sticky="nesw")
                atlbl3.grid(row=1,column=0, sticky="nesw")
                atlbl4.grid(row=1,column=1, sticky="nesw")
                atlbl5.grid(row=2,column=0, sticky="nesw")
                atlbl6.grid(row=2,column=1, sticky="nesw")


                ### Right Menus
                options = tk.LabelFrame(lf2,text="YOUR OPTIONS")
                options.pack(fill="x",expand=True,side="top")
                mabtn = tk.Button(options,text="Mark Attendance",bg="green",fg="white",relief="flat",command=self.mark_attendance)
                mabtn.pack(fill="x",expand="yes",pady=2)

                vabtn = tk.Button(options,text="View Attendance",bg="orange",fg="white",relief="flat",command=self.get_attendance)
                vabtn.pack(fill="x",expand="yes",pady=2)

                lgbtn = tk.Button(options,text="Log Out",bg="red",fg="white",relief="flat",command=self.logout)
                lgbtn.pack(fill="x",expand="yes",pady=2)

                btn = tk.Button(options,text="Refresh",command=self.profile.update)
                btn.pack()

                # VIEW ATTENDANCE
                self.options1 = ttk.Frame(lf2)
                self.profile.geometry("700x700")

                self.canvas=tk.Canvas(self.options1,bg='#FFFFFF',width=300,height=1000,scrollregion=(0,0,300,1000))
                self.canvas.pack(side="left",expand=True,fill="both")

                scrollbar = ttk.Scrollbar(self.options1,command=self.canvas.yview)
                
                self.scrollable_frame = ttk.Frame(self.canvas,width=300)
                self.scrollable_frame.pack(fill="x",expand="yes")
                
                self.scrollable_frame.bind(
                "<Configure>",
                lambda e: self.canvas.configure(
                        scrollregion=self.canvas.bbox("all")
                )
                )
                self.canvas.create_window((0, 0), window=self.scrollable_frame, anchor="nw",width=320)
                # scrollbar.config()
                self.options1.pack(fill="both",expand=True,side="top")
                self.canvas.config(yscrollcommand=scrollbar.set)
                
                scrollbar.pack(side="right", fill="y")
                self.profile.resizable(0, 0)
                self.profile.mainloop()

        def mark_attendance(self):
                uid = self.dt[0]
                date = datetime.now().strftime("%d-%m-%Y")
                time = datetime.now().time()
                status = "P"
                wd = datetime.now().strftime("%A")
                att = update_attendance(uid,date,time,status,wd)
                if att["status"]== True:
                        messagebox.showinfo("Attendance System","Attendance marked successfully!! on: {} at: {}".format(date,time))

                else:
                        rc = att["data"]
                        messagebox.showwarning("Attendance System","You already have marked today's attendance at: {}".format(rc[0][3].split(".")[0]))
        
        @run_once
        def get_attendance(self):
                result=fetch_attendance(self.dt[0])
                c=1
                dic = {"P":"Present","A":"Absent"}
                for i in result:
                        color = "red"
                        if i[4]=="P":
                                color="green"
                        lbl = "lbl"+str(c)
                        ab = "{} \n{} \n({}) \n {}".format(i[2],i[3].split(".")[0],i[5],dic[i[4]])
                        lbl = ttk.Label(self.scrollable_frame, text=ab,background=color,foreground="white",font=("Helvetica",14),anchor="center")    
                        lbl.pack(fill="x",ipadx=5,ipady=5,padx=5,pady=5,expand="yes")
                        c+=1      

        def logout(self):
                del self.dt
                self.profile.destroy()