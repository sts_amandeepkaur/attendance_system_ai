import face_recognition
from database_code import *
import cv2
def capture():
    faceDetect=cv2.CascadeClassifier('xml/frontal_face.xml')
    cam = cv2.VideoCapture(0)
    cv2.namedWindow("Attendance System", cv2.WINDOW_NORMAL)
    #s=(70,30)
    while True:
        ret,img = cam.read()
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        faces = faceDetect.detectMultiScale(gray,1.2)
        cv2.putText(img,"Press c to exit",(0,450),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,255),1)
        cv2.putText(img,"Press m to continue",(0,400),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,255),2)
        action= cv2.waitKey(10)
        if action==ord('m'):
            try:
                x,y,w,h=faces[0]
                face=img[y: y + h, x : x + w ]
                cam.release() 
                cv2.destroyAllWindows()
                return face
            except IndexError:
                return ["IndexError"]
        if action== ord('c'):
            cam.release()
            cv2.destroyAllWindows()
            return 0
        
        if len(faces) > 1:
            cv2.putText(img,"There must be one face only",(50,50),cv2.FONT_HERSHEY_COMPLEX,1,(0,0,255),2)

        if len(faces) < 1:
            cv2.putText(img,"There must be one atleast one face",(50,50),cv2.FONT_HERSHEY_COMPLEX,1,(0,0,255),2)

        for (x,y,w,h) in faces:
            cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
            
        cv2.imshow("Attendance System",img)
        
    cam.release()
    cv2.destroyAllWindows()


def compare_face(roll_no):
    import cv2
    image=capture()
    data = search(roll_no)
    if image == ["IndexError"]:
        return {"match":None}
    if data==0:
        return {"match":"Not Found"}
    else:
        data = data[0]
        name,roll = data[1],data[2]
        fl = "{}__{}.jpg".format(name,roll)
        real_image = face_recognition.load_image_file("images/"+fl)
        real_image1 = face_recognition.face_encodings(real_image)[0]

        image1 = face_recognition.face_encodings(image)[0]
        comp = face_recognition.compare_faces([real_image1],image1)
       
        if comp[0]==True:
            return {"match":True,"data": data}
        else:
            return {"match":False,"data":data}
        