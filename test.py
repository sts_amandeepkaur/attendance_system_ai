import tkinter as tk
# from PIL import Image, ImageTk
# root1 = tk.Tk()
# root1.title("Attendance System")
# name,r_no = "a","1"
# # pic= "{}__{}.jpg".format(name,r_no)

# lb = tk.Label(root1,text="Welcome {} !!!".format(name), font=("Verdena",20), fg="green",bg="white")
# lb.pack(ipadx=10,ipady=5,padx=10,pady=10,fill="x")


# photo_location = Image.open("images/"+"Amandeep__1502238.jpg")
# tkpi = ImageTk.PhotoImage(photo_location)
# label_image = tk.Label(root1, image=tkpi)
# label_image.image = tkpi
# label_image.pack()

# btn =tk.Button(root1,text="Click to Continue",bg="green",fg="white",bd=0)
# btn.pack(ipadx=10,ipady=5,padx=10,pady=10,fill="x")

# btn1 =tk.Button(root1,text="Click to Cancel",bg="red",fg="white",bd=0,command=exit)
# btn1.pack(ipadx=10,ipady=5,padx=10,pady=10,fill="x")

# root1.geometry("500x500")
# root1.resizable(0,0)

# root1.mainloop()
# import re
# d=(5, 'Amandeep', 1502238, 'B.Tech', 'None', 'None', '2020-03-20 12:31:29.098738')
# dt=list(map(lambda x:str(x).split(" ") if re.match('\d{4}-\d{2}-\d{2}', str(x)) else x,d))
# ls=[]
# for i in dt:
#     if type(i)==type([]):
#         for j in i:
#             ls.append(j.split(".")[0])
#     else:
#         ls.append(i)
# print(ls)

# import datetime
# print(datetime.datetime.now().strftime("%d-%m-%Y"))
# print(datetime.datetime.now().time())
# print(dir(datetime.datetime.now())
# )
# print(datetime.datetime.now().strftime("%A")
# )
import tkinter as tk
from tkinter import ttk

root = tk.Tk()
container = ttk.Frame(root)
canvas = tk.Canvas(container)
scrollbar = ttk.Scrollbar(container, orient="vertical", command=canvas.yview)
scrollable_frame = ttk.Frame(canvas)

scrollable_frame.bind(
    "<Configure>",
    lambda e: canvas.configure(
        scrollregion=canvas.bbox("all")
    )
)

canvas.create_window((0, 0), window=scrollable_frame, anchor="nw")

canvas.configure(yscrollcommand=scrollbar.set)

for i in range(50):
    ttk.Label(scrollable_frame, text="Sample scrolling label").pack()

canvas.pack(side="left", fill="both", expand=True)
scrollbar.pack(side="right", fill="y")

root.mainloop()