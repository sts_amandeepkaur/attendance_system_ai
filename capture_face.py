import cv2
import matplotlib.pyplot as plt
import tkinter
from tkinter import messagebox
from database_code import insert_data
def plot_show(image,title=""):
    if len(image.shape) == 3:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) #opencv supportsBGR, matplotlib supports RGB
    plt.axis("off")
    plt.title(title)
    plt.imshow(image,cmap="Greys_r")
    plt.show()

def capture(lbl,crse):
    faceDetect=cv2.CascadeClassifier('xml/frontal_face.xml')
    cam = cv2.VideoCapture(0)
    cv2.namedWindow("Attendance System", cv2.WINDOW_NORMAL)
    #s=(70,30)
    while True:
        ret,img = cam.read()
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        faces = faceDetect.detectMultiScale(gray,1.2)
        cv2.putText(img,"Press c to exit",(0,450),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,255),1)
        cv2.putText(img,"Press s to save image",(0,400),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,255),2)
        action= cv2.waitKey(10)
        if action==ord('s'):
            try:
                x,y,w,h=faces[0]
                face=img[y: y + h, x : x + w ]
                cv2.imwrite( 'images/' + str(lbl)+'.jpg',face)
                

                nm_rno = lbl.split("__")
                save_data = insert_data(nm_rno[0],str(nm_rno[1]),course=crse)
                cam.release() 
                cv2.destroyAllWindows()

                if save_data:
                    return {"registred":True,"face":face,"name":nm_rno[0]}
                else:
                    return {"registred":False}
            except IndexError:
                return {"registred":None}

        if action== ord('c'):
            cam.release()
            cv2.destroyAllWindows()
            return 0
        
        if len(faces) > 1:
            cv2.putText(img,"There must be one face only",(50,50),cv2.FONT_HERSHEY_COMPLEX,1,(0,0,255),2)

        if len(faces) < 1:
            cv2.putText(img,"There must be one atleast one face",(50,50),cv2.FONT_HERSHEY_COMPLEX,1,(0,0,255),2)

        for (x,y,w,h) in faces:
            cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
            
        cv2.imshow("Attendance System",img)
        
    cam.release()
    cv2.destroyAllWindows()
