import pymysql
import datetime
from tkinter import messagebox
db = pymysql.connect(
    host="localhost",
    user="root",
    password="test",
    database="attendance_system_ai"
)
def insert_data(name,roll_no,course=None,branch=None,sem=None):
    cur = db.cursor()
    dt = datetime.datetime.now()
    try:
        qry = "INSERT INTO register(student_name,roll_number,course,branch,semester,registration_date_time) \
            VALUES('{}','{}','{}','{}','{}','{}')".format(name,roll_no,course,branch,sem,str(dt))
        cur.execute(qry)
        db.commit()
        return True
    except:
        return False
def search(rno):
    cur = db.cursor()
    qry= "SELECT * FROM register WHERE roll_number={}".format(rno)
    cur.execute(qry)
    data = cur.fetchall()
    if len(data)==0:
        return 0
    else:
        return data

def update_attendance(uid,dt,tm,st,wkd):
    update_all(uid,dt,tm,"A",wkd)
    cur = db.cursor()
    qry = "SELECT * FROM attendance WHERE userid='{}' AND date='{}' AND status='{}'".format(uid,dt,st)
    cur.execute(qry)
    record = cur.fetchall()
    if len(record)==0:
        cur1 = db.cursor()
        qry1 = "UPDATE attendance SET time='{}', status='{}' WHERE userid='{}' AND date='{}'".format(tm,st,uid,dt)
        cur1.execute(qry1)
        db.commit()
        return {"status":True,"data":[]}
        
    else:
        return {"status":False,"data":record}

def update_all(user_id,date,time,status,wkday):
    cur = db.cursor()
    qry = "SELECT id FROM register"
    cur.execute(qry)
    all_ids =cur.fetchall()

    cur1 = db.cursor()
    qry1 ="SELECT * FROM attendance WHERE date='{}'".format(date)
    cur1.execute(qry1)
    att = cur1.fetchall()
    if len(att)==0:
        for i in all_ids:
            cur2 = db.cursor()
            qry2 = "INSERT INTO attendance(userid,date,time,status,Weekday) VALUES('{}','{}','{}','{}','{}')".format(i[0],date,time,status,wkday)
            cur2.execute(qry2)
            db.commit()


def fetch_attendance(uid):
    cur = db.cursor()
    qry ="SELECT * FROM attendance WHERE userid={}".format(uid)
    cur.execute(qry)
    result=cur.fetchall()
    return result

def fetch_attendance_prc(uid):
    mcur = db.cursor()
    qry ="SELECT * FROM attendance WHERE userid={}".format(uid)
    mcur.execute(qry)
    result=mcur.fetchall()
    mcur1 = db.cursor()
    qry1 ="SELECT * FROM attendance WHERE userid={} and status='P'".format(uid)
    mcur1.execute(qry1)
    result1=mcur1.fetchall()
    return {"total":len(result),"present":len(result1)}

